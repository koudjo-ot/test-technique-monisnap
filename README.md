# Run locally

-   fork develop branch
-   copy .env_local into your .env && copy server/.env_local into you server/.env

# ENV

-   REACT_APP_API_LOCAL= Your back end url

# /server/.env

-   URL_API_STAR_WARS=
-   TYPE_ORM_URL_BDD=

# START

-   yarn install
-   yarn dev

# TEST

-   yarn cypress:navigator - test E2E cypress

*   yarn test - jest

# Exercices

-   You can change the test value by editing "editThisValue"'s variable in script1 et script2

*   ./src/exercices/script1.js
*   ./src/exercices/script2.js

# Run exercice 1, exercice 2

-   yarn exo1
-   yarn exo2

# Front url deployement

-   https://front-test-monisnap.herokuapp.com
