module.exports = {
	name: 'default',
	type: 'postgres',
	url: process.env.TYPE_ORM_URL_BDD,
	synchronize: true,
	logging: true,
	entities: ['dist/**/*.entity{.ts,.js}'],
};
