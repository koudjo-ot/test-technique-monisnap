import { ApiProperty } from '@nestjs/swagger';
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'characters votes' })
export class CharactersVotesEntity {
	@ApiProperty({ type: Number, description: 'id' })
	@PrimaryGeneratedColumn()
	idBdd: number;

	@ApiProperty({ type: String, description: 'character name' })
	@Column()
	name: string;

	@ApiProperty({ type: Number, description: 'numbers of votes' })
	@Column()
	votes: number;

	@ApiProperty({ type: String, description: 'id google' })
	@Column()
	idMonisnap: number;
}
