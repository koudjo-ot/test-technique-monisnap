import { ApiProperty } from '@nestjs/swagger';
import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	Unique,
} from 'typeorm';

@Entity({ name: 'characters' })
@Unique(['idMonisnap'])
export class CharactersEntity {
	@ApiProperty({ type: Number, description: 'id' })
	@PrimaryGeneratedColumn()
	id: number;

	@ApiProperty({ type: String, description: 'character name' })
	@Column()
	name: string;

	@ApiProperty({ type: String, description: 'picture' })
	@Column()
	pic: string;

	@ApiProperty({ type: String, description: 'homeworld' })
	@Column({ nullable: true })
	homeworld: string;

	@ApiProperty({ type: Number, description: 'numbers of votes' })
	@Column()
	votes: number;

	@ApiProperty({ type: String, description: 'id api star wars' })
	@Column()
	idMonisnap: number;
}
