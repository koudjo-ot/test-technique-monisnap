export const characterTopUnordered = [
	{
		idMonisnap: 21,
		name: 'Palpatine',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/d/d8/Emperor_Sidious.png',
		homeworld: 'naboo',
		votes: 2,
		id: 34895,
	},
	{
		idMonisnap: 1,
		name: 'Luke Skywalker',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/2/20/LukeTLJ.jpg',
		homeworld: 'tatooine',
		votes: 622,
		id: 3489,
	},
	{
		idMonisnap: 18,
		name: 'Wedge Antilles',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/6/60/WedgeHelmetless-ROTJHD.jpg',
		homeworld: 'corellia',
		votes: 122,
		id: 34892,
	},
	{
		idMonisnap: 2,
		name: 'C-3PO',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/3/3f/C-3PO_TLJ_Card_Trader_Award_Card.png',
		homeworld: 'tatooine',
		votes: 422,
		id: 34891,
	},
	{
		idMonisnap: 19,
		name: 'Jek Tono Porkins',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/e/eb/JekPorkins-DB.png',
		homeworld: 'bestine',
		votes: 212,
		id: 34893,
	},
	{
		idMonisnap: 20,
		name: 'Yoda',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/d/d6/Yoda_SWSB.png',
		votes: 222,
		id: 34894,
	},
];

export const characterTopOrdered = [
	{
		idMonisnap: 1,
		name: 'Luke Skywalker',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/2/20/LukeTLJ.jpg',
		homeworld: 'tatooine',
		votes: 622,
		id: 3489,
	},
	{
		idMonisnap: 2,
		name: 'C-3PO',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/3/3f/C-3PO_TLJ_Card_Trader_Award_Card.png',
		homeworld: 'tatooine',
		votes: 422,
		id: 34891,
	},
	{
		idMonisnap: 18,
		name: 'Wedge Antilles',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/6/60/WedgeHelmetless-ROTJHD.jpg',
		homeworld: 'corellia',
		votes: 122,
		id: 34892,
	},
	{
		idMonisnap: 19,
		name: 'Jek Tono Porkins',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/e/eb/JekPorkins-DB.png',
		homeworld: 'bestine',
		votes: 212,
		id: 34893,
	},
	{
		idMonisnap: 20,
		name: 'Yoda',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/d/d6/Yoda_SWSB.png',
		votes: 222,
		id: 34894,
	},
	{
		idMonisnap: 21,
		name: 'Palpatine',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/d/d8/Emperor_Sidious.png',
		homeworld: 'naboo',
		votes: 2,
		id: 34895,
	},
];
