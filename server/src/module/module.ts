export function splitIntoChunk(arr, chunk) {
	const listBattle = [];
	for (let i = 0; i < arr.length; i += chunk) {
		let tempArray;
		const oddArr = arr.length < i + chunk;
		if (oddArr) {
			tempArray = [arr[i], arr[0]];
		} else {
			tempArray = arr.slice(i, i + chunk);
		}
		listBattle.push(tempArray);
	}
	return listBattle;
}

export function getAllCharactersSorted(listChar) {
	return [...listChar].sort((a, b) => a.idMonisnap - b.idMonisnap);
}
