import {
	characterTopUnordered,
	characterTopOrdered,
} from './fakeOrderedList';
import { getAllCharactersSorted, splitIntoChunk } from './module';

it('SHOULD return an ascending sorted List by Id', () => {
	const sorted = getAllCharactersSorted(characterTopUnordered);
	expect(sorted.map((char) => char.idMonisnap)).toEqual(
		characterTopOrdered.map((char) => char.idMonisnap),
	);
});

it('SHOULD return a couple array', () => {
	const battleCouple = splitIntoChunk(characterTopOrdered, 2);
	expect(battleCouple.length).toEqual(
		characterTopOrdered.length / 2,
	);
});

it('SHOULD return a couple array even if odd length', () => {
	const battleCouple = splitIntoChunk([1, 2, 3, 4, 5, 6, 7], 2);
	expect(battleCouple).toEqual([
		[1, 2],
		[3, 4],
		[5, 6],
		[7, 1],
	]);
});
