import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { CharactersModule } from 'src/characters/characters.module';
require('dotenv').config();

@Module({
	imports: [
		TypeOrmModule.forRoot(),
		ConfigModule.forRoot({ isGlobal: true }),
		CharactersModule,
	],
	controllers: [],
	providers: [],
})
export class AppModule {}
