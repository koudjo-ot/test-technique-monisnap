import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app/app.module';
require('dotenv').config();

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.enableCors();
	if (process.env.NODE_ENV !== 'production') {
		const options = new DocumentBuilder()
			.setTitle('MONISNAP TEST TECHNIQUE')
			.setDescription('MONISNAP')
			.setVersion('0.0.0')
			.build();
		const document = SwaggerModule.createDocument(app, options);
		SwaggerModule.setup('api', app, document);
	}
	await app.listen(process.env.PORT || 8080);
}
bootstrap();
