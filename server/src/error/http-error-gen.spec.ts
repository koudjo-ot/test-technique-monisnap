import { httpErrorGen } from './http-error-gen';

describe('Error Generator http Exceptioin', () => {
  it('should trow http Exception', async () => {
    const error = { isValid: false, errStatus: '404', message: 'error 404' };
    expect(() => {
      httpErrorGen(error);
    }).toThrow(error.message);
  });
});
