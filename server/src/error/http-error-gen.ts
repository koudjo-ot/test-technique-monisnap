import { HttpException, HttpStatus } from '@nestjs/common';

export interface IsValid {
  isValid?: boolean;
  errStatus: HttpStatus;
  message: string;
}
export function httpErrorGen({ isValid = false, errStatus, message }) {
  if (!isValid) {
    throw new HttpException(
      {
        status: errStatus,
        message,
      },
      errStatus,
    );
  }
}
