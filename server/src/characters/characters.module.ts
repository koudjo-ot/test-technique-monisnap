import { Module } from '@nestjs/common';
import { CharactersController } from './characters.controller';
import { CharactersService } from './characters.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CharactersEntity } from 'entities/charactersEntity.entity';

@Module({
	imports: [TypeOrmModule.forFeature([CharactersEntity])],
	controllers: [CharactersController],
	providers: [CharactersService],
})
export class CharactersModule {}
