import { ApiProperty } from '@nestjs/swagger';

export class Character {
	@ApiProperty()
	id: number;

	name: string;
	pic: string;
	homeworld: string;
	votes: number;
}

export class AllCharactersDTO {
	@ApiProperty()
	status: number;

	characters: [Character];
}
