import {
	Controller,
	Get,
	Param,
	HttpCode,
	Put,
} from '@nestjs/common';
import {
	ApiCreatedResponse,
	ApiNotFoundResponse,
	ApiOkResponse,
} from '@nestjs/swagger';
import { CharactersService } from './characters.service';

@Controller('characters')
export class CharactersController {
	// eslint-disable-next-line no-useless-constructor
	constructor(private charactersService: CharactersService) {}
	@Get()
	@ApiOkResponse({ description: 'All Characters' })
	@ApiNotFoundResponse({
		description: 'Characters do not exist',
	})
	@HttpCode(200)
	async getAllCharacters() {
		const characters =
			await this.charactersService.getAllCharacters();
		return characters;
	}

	@HttpCode(200)
	@Get('/battles')
	async getAllBattles() {
		const characters =
			await this.charactersService.getAllBattles();
		return characters;
	}

	@Put('/vote/:id')
	@ApiCreatedResponse({
		status: 204,
		description: 'Vote added',
	})
	@ApiNotFoundResponse({ description: 'Character does not exist' })
	@HttpCode(204)
	public updateVoteCharById(@Param('id') id: string) {
		return this.charactersService.updateVoteCharById(id);
	}
}
