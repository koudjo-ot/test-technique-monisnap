import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CharactersEntity } from 'entities/charactersEntity.entity';
import { getConnection, Repository } from 'typeorm';
import {
	getAllCharactersSorted,
	splitIntoChunk,
} from 'src/module/module';
import { starWarsChar } from 'src/star_wars_characters';

@Injectable()
export class CharactersService {
	// eslint-disable-next-line no-useless-constructor
	constructor(
		@InjectRepository(CharactersEntity)
		private charactersEntityRepo: Repository<CharactersEntity>,
	) {}

	getAllCharacters = async (): Promise<CharactersEntity[]> => {
		const getAllCharacters: CharactersEntity[] =
			await this.charactersEntityRepo.find();
		return getAllCharacters;
	};

	updateVoteCharById = async (id) => {
		const getChar: CharactersEntity =
			await this.charactersEntityRepo.findOneOrFail(id);
		if (getChar) {
			const updatedVote = getChar?.votes + 1;
			await getConnection()
				.createQueryBuilder()
				.update(CharactersEntity)
				.set({ votes: updatedVote })
				.where('id = :id', { id: id })
				.execute();
		}
	};

	getAllBattles = async (): Promise<any> => {
		try {
			const checkIfAllCharactersExist: CharactersEntity[] =
				await this.charactersEntityRepo.find();

			if (
				checkIfAllCharactersExist.length !==
				starWarsChar?.characters.length // prevent API expiration
			) {
				// first launch app copy data from API
				const newAddedCharacter = [];
				for (
					let index = 0;
					index < starWarsChar?.characters.length;
					index += 1
				) {
					const newCharacters: CharactersEntity =
						new CharactersEntity();
					newCharacters.name =
						starWarsChar?.characters[index].name;
					newCharacters.pic =
						starWarsChar?.characters[index].pic;
					newCharacters.homeworld =
						starWarsChar?.characters[index].homeworld;
					newCharacters.votes = 0;
					newCharacters.idMonisnap =
						starWarsChar?.characters[index].id;
					newAddedCharacter.push(newCharacters);
				}

				await this.charactersEntityRepo
					.createQueryBuilder()
					.insert()
					.into(CharactersEntity)
					.values(newAddedCharacter)
					.orIgnore(`("idMonisnap") DO NOTHING`)
					.execute();
			}

			const getAllCharacters: CharactersEntity[] =
				await this.charactersEntityRepo.find();
			const sortedCharacters =
				getAllCharactersSorted(getAllCharacters);
			const charactersBattles = splitIntoChunk(
				sortedCharacters,
				2,
			);
			return charactersBattles;
		} catch (error) {
			console.log(error);
		}
	};
}
