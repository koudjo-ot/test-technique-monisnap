import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { device } from './mediaQueryBreakPoints';

type FlexProps = {
	flexFlow?: string;
	justifyContent?: string;
	margin?: string;
	padding?: string;
};

type TextProps = {
	fontSize?: string;
	color?: string;
	margin?: string;
	textalign?: string;
	lineHeight?: string;
	fontWeight?: string;
	textDecoration?: string;
	cursor?: string;
};

export const Container = styled.section<FlexProps>`
	display: flex;
	justify-content: center;
`;
export const Wrapper = styled.div<FlexProps>`
	display: flex;
	flex-flow: ${({ flexFlow }) => flexFlow || 'row wrap'};
	justify-content: ${({ justifyContent }) =>
		justifyContent || 'flex-start'};
	margin: ${({ margin }) => margin || '2rem 0 2rem 0'};
	padding: ${({ padding }) => padding || '0 2rem 0 2rem'};
`;

export const Paragraph = styled.p<TextProps>`
	font-size: ${({ fontSize }) => fontSize || '1rem'};
`;

export const StyledSpan = styled.span<TextProps>`
	color: ${({ color }) => color || '#979797'};
	margin: ${({ margin }) => margin || '1rem 0 1rem 0'};
	line-height: ${({ lineHeight }) => lineHeight || '1rem'};
	text-align: ${({ textalign }) => textalign || 'left'};
	font-size: ${({ fontSize }) => fontSize || '0.9rem'};
	font-weight: ${({ fontWeight }) => fontWeight || 'normal'};
	text-decoration: ${({ textDecoration }) =>
		textDecoration || '#4169e1 wavy underline;'};
	cursor: ${({ cursor }) => cursor || 'auto'};
`;

export const StyledLink = styled(Link)<TextProps>`
	color: ${({ color }) => color || '#979797'};
	margin: ${({ margin }) => margin || '1rem 0 1rem 0'};
	line-height: ${({ lineHeight }) => lineHeight || '1rem'};
	text-align: ${({ textalign }) => textalign || 'left'};
	font-size: ${({ fontSize }) => fontSize || '0.9rem'};
	font-weight: ${({ fontWeight }) => fontWeight || 'normal'};
	text-decoration: ${({ textDecoration }) =>
		textDecoration || 'none'};
	cursor: ${({ cursor }) => cursor || 'auto'};
`;
interface BoxProps {
	flexFlow?: string;
	margin?: string;
	alignItems?: string;
	justifyContent?: string;
}
export const Box = styled.div`
	display: flex;
	flex-flow: ${({ flexFlow }: BoxProps) =>
		flexFlow || 'column wrap'};
	margin: ${({ margin }: BoxProps) => margin || '2rem'};
	width: 300px;
	justify-content: ${({ justifyContent }: BoxProps) =>
		justifyContent || 'end'};
	align-items: ${({ alignItems }: BoxProps) =>
		alignItems || 'unset'};
	@media ${device.mobileS} {
		width: 200px;
		padding: 0;
	}

	@media ${device.laptopM} {
		max-width: 300px;
	} ;
`;

export const ErrorInput = styled.span`
	color: red;
	font-size: 0.8rem;
	margin-top: 0.5rem;
`;

interface ImageProps {
	isLeft?: string;
}
export const ImageLogo = styled.img<ImageProps>`
	position: absolute;
	${({ isLeft }) => (isLeft ? 'left: 10px;' : 'right: 50px;')}
	${({ isLeft }) => (isLeft ? 'top: 15px;' : 'top: 20px;')}
`;

export const TitleH2 = styled.h2`
	margin-top: 0;
`;

export const Relative = styled.div`
	margin: ${({ margin }: BoxProps) => margin || ' 0 0 0 0'};
	position: relative;
	@media ${device.laptopM} {
		width: 300px;
	}
	@media ${device.mobileS} {
		width: 200px;
		padding: 0;
	} ;
`;

export const StyledForm = styled.form`
	display: flex;
	flex-flow: column wrap;
	margin: 1rem;
	${Box} {
		padding-bottom: 1rem;
	}
	${Box}:last-child {
		padding-bottom: 0;
	}
	justify-content: center;
	align-content: center;
	height: auto;
	min-width: 440px;
	max-width: 700px;
	width: unset;
	padding: 55px 55px 37px;
	background: #fff;
	border-radius: 250px 750px 250px 750px / 750px 250px 750px 250px;
	animation: wobble 15s ease-in-out alternate infinite;
	@keyframes wobble {
		50% {
			border-radius: 750px 550px 350px 750px / 350px 750px 550px
				450px;
		}
		100% {
			border-radius: 750px 250px 750px 250px / 250px 750px 250px
				750px;
		}
	}
	@media ${device.laptopM} {
		align-content: center;
		padding: 2rem 0 2rem 0;
		text-align: center;
	}
	@media ${device.mobileL} {
		min-width: 100vw;
	}
`;
