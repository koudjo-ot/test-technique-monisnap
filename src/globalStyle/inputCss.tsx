import styled from 'styled-components';
import { device } from './mediaQueryBreakPoints';
interface InputProps {
	titleLabel?: string;
	type?: string;
	id?: string;
	margin?: string;
	errors?: any;
	placeholder?: string;
	showLogo?: string;
	logoIsLeft?: string;
	altText?: string;
}

export const StyledInput = styled.input<InputProps>`
	height: 40px;
	@media ${device.laptopM} {
		width: inherit;
	}
	&:focus {
		outline: none !important;
		border: 2px solid black;
		box-shadow: 0 0 8px #343b3e;
		transition: border 0.1s;
	}
`;

type LabelProps = {
	margin?: string;
	justifyContent?: string;
	width?: string;
};

export const StyledLabel = styled.label<LabelProps>`
	display: flex;
	flex-flow: column wrap;
	width: 100%;
	margin: ${({ margin }) => margin || '0 0 1rem 0'};
	@media ${device.laptopM} {
		width: inherit;
		text-align: left;
		align-content: center;
	}
`;

export const ErrorInput = styled.div`
	color: red;
	font-size: 0.8rem;
	margin-top: 0.5rem;
`;

export type ContainerProps = {
	justifyContent?: string;
	width?: string;
	alignSelf?: string;
	flexFlow?: string;
};

export const Container = styled.div<ContainerProps>`
	display: flex;
	flex-flow: ${({ flexFlow }) => flexFlow || 'row wrap'};
	width: ${({ width }) => width || '100%'};
	align-self: ${({ alignSelf }) => alignSelf || 'auto'};
	justify-content: ${({ justifyContent }) =>
		justifyContent || 'center'};
`;
