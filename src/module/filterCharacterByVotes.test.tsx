import { filterCharacterByVotes } from './filterCharacterByVotes';

const fakeValues = [
	{
		id: 1,
		name: 'Luke Skywalker',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/2/20/LukeTLJ.jpg',
		homeworld: 'tatooine',
		votes: 622,
	},
	{
		id: 2,
		name: 'C-3PO',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/3/3f/C-3PO_TLJ_Card_Trader_Award_Card.png',
		homeworld: 'tatooine',
		votes: 2,
	},
	{
		id: 32,
		name: 'C-3PO',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/3/3f/C-3PO_TLJ_Card_Trader_Award_Card.png',
		homeworld: 'tatooine',
		votes: 12,
	},
	{
		id: 21,
		name: 'Palpatine',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/d/d8/Emperor_Sidious.png',
		homeworld: 'naboo',
		votes: 22,
	},
];

const resultValues = [
	{
		id: 1,
		name: 'Luke Skywalker',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/2/20/LukeTLJ.jpg',
		homeworld: 'tatooine',
		votes: 622,
	},
	{
		id: 21,
		name: 'Palpatine',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/d/d8/Emperor_Sidious.png',
		homeworld: 'naboo',
		votes: 22,
	},
	{
		id: 32,
		name: 'C-3PO',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/3/3f/C-3PO_TLJ_Card_Trader_Award_Card.png',
		homeworld: 'tatooine',
		votes: 12,
	},
	{
		id: 2,
		name: 'C-3PO',
		pic: 'https://vignette.wikia.nocookie.net/starwars/images/3/3f/C-3PO_TLJ_Card_Trader_Award_Card.png',
		homeworld: 'tatooine',
		votes: 2,
	},
];
it('SHOULD return an ascending values', () => {
	const characters = filterCharacterByVotes(fakeValues, {
		asc: true,
	});
	expect(characters).toStrictEqual(resultValues);
});

it('SHOULD return an descending values', () => {
	const characters = filterCharacterByVotes(fakeValues, {
		asc: false,
	});
	const expectedArray = [...resultValues].reverse();
	expect(characters).toStrictEqual(expectedArray);
});
