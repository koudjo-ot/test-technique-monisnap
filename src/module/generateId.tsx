function generateId(name: number | string) {
	return `${name}_${Math.floor(
		Math.random() * 100000,
	)}${Math.random().toString(36).substr(2, 5)}`;
}

export default generateId;
