export const capitalizeAllString = (word: string): string => {
	if (!word) return word;
	return word.toUpperCase();
};

export const capitalizeFirstChar = (word: string): string => {
	if (!word) return word;
	const regex = /^\D/g;

	return word.replace(regex, (letter) => letter.toUpperCase());
};
