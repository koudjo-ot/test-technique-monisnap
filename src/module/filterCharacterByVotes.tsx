export interface Order {
	asc?: boolean;
	title?: string;
}
export const filterCharacterByVotes = (
	rawListCharacters: any,
	{ asc }: Order,
) => {
	return rawListCharacters.sort((a: any, b: any) =>
		asc === true ? b.votes - a.votes : a.votes - b.votes,
	);
};
