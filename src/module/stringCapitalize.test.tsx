import {
	capitalizeAllString,
	capitalizeFirstChar,
} from './stringCapitalize';

it('SHOULD CAPITALIZE ALL WORD', () => {
	const word = 'GUTS';
	expect(capitalizeAllString('guts')).toBe(word);
});

it('SHOULD CAPITALIZE FIRST LETTER', () => {
	const word = 'Guts';
	expect(capitalizeFirstChar('guts')).toBe(word);
});

it('SHOULD NOT CAPITALIZE FIRST LETTER', () => {
	const word = '7guts';
	expect(capitalizeFirstChar('7guts')).toBe(word);
});
