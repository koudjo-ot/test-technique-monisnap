import React from 'react';

import styled from 'styled-components';

import { useRoutes } from 'react-router-dom';
import { routes } from './Info/allPage';

const MainContainer = styled.main`
	text-align: center;
	display: flex;
	flex-direction: column;
	justify-content: center;
`;
function App() {
	const element = useRoutes(routes);
	return <MainContainer>{element}</MainContainer>;
}

export default App;
