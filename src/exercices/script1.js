function checkIfAllNumbZerotoNine(numb) {
	const expect = '0123456789';
	const numbStr = numb.toString();
	const str = numbStr.split('').sort();

	const solution = Array.from(new Set(str)).join('');

	return solution === expect;
}

const editThisValue = 98410783562910;

console.log(checkIfAllNumbZerotoNine(editThisValue));
