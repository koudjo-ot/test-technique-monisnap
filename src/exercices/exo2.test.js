import { checkAlphabet } from './exo2';

it('should find letter miss', () => {
	expect(checkAlphabet(['a', 'b', 'd'])).toEqual(['c']);
});

it('should return array without modification', () => {
	expect(checkAlphabet(['a'])).toEqual(['a']);
});

it('should return the value without modification', () => {
	expect(checkAlphabet('a')).toEqual('a');
});

it('should find letter miss in an unsorted array', () => {
	expect(checkAlphabet(['d', 'b', 'a'])).toEqual(['c']);
});

it('should find letter miss in an unsorted array with UpperCase', () => {
	expect(checkAlphabet(['D', 'b', 'A'])).toEqual(['c']);
});

it('should do nothing if there are numbers', () => {
	expect(checkAlphabet([9, 1, 66])).toEqual([9, 1, 66]);
});
