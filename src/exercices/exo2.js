export function checkAlphabet(list) {
	if (!Array.isArray(list) || list.length < 2) return list;

	const nextStep = list.every(
		(currentValue) => typeof currentValue === 'string',
	);
	if (!nextStep) return list;
	const sortedList = list.join('').toLowerCase().split('').sort();

	const letters = [
		'a',
		'b',
		'c',
		'd',
		'e',
		'f',
		'g',
		'h',
		'i',
		'j',
		'k',
		'l',
		'm',
		'n',
		'o',
		'p',
		'q',
		'r',
		's',
		't',
		'u',
		'v',
		'w',
		'x',
		'y',
		'z',
	];
	const firsLetter = letters.findIndex(
		(element) => element === sortedList[0],
	);
	const lastLetter =
		letters.findIndex(
			(element) => element === sortedList[list.length - 1],
		) + 1;

	const sequenceOfAlphabet = letters.slice(firsLetter, lastLetter);
	const missingLetter = sequenceOfAlphabet.filter((item) => {
		return !sortedList.includes(item);
	});

	return missingLetter;
}
