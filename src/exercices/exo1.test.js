import { checkIfAllNumbZerotoNine } from './exo1';

it('should have all number', () => {
	expect(checkIfAllNumbZerotoNine(98410783562910)).toBeTruthy();
});

it('should not have all number', () => {
	expect(checkIfAllNumbZerotoNine(984178356291)).toBeFalsy();
});

it('should have all number duplicates BigInt', () => {
	expect(
		checkIfAllNumbZerotoNine(98765432109876543210n),
	).toBeTruthy();
});

it('should have all number Bigint', () => {
	expect(
		checkIfAllNumbZerotoNine(99887766554433221100n),
	).toBeTruthy();
});

it('should not work because a lose of precision', () => {
	expect(
		// eslint-disable-next-line no-loss-of-precision
		checkIfAllNumbZerotoNine(99887766554433221100),
	).toBeFalsy();
});
