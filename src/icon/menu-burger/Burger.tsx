import React from 'react';
import PropTypes from 'prop-types';

interface ProptypesInterface {
	color?: string;
	handleClick?: () => void;
}
const Burger = ({ color, handleClick }: ProptypesInterface) => {
	return (
		<svg
			viewBox="0 0 100 80"
			width="40"
			height="40"
			onClick={handleClick}
		>
			<rect width="100" height="20" rx="8" fill={color}></rect>
			<rect
				y="30"
				width="100"
				height="20"
				rx="8"
				fill={color}
			></rect>
			<rect
				y="60"
				width="100"
				height="20"
				rx="8"
				fill={color}
			></rect>
		</svg>
	);
};

Burger.propTypes = {
	color: PropTypes.string,
};
export default Burger;
