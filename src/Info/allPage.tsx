import type { RouteObject } from 'react-router-dom';
import DashboardView from '../Component/dashboard/Dashboard';
import TopPerso from '../Component/dashboard/TopPerso';
import PageNotFound from '../Component/ui/PageNotFound';

export interface ListOfPage {
	titre?: string;
	lien?: string;
	composant?: any;
	exact?: boolean;
}

export const routes: RouteObject[] = [
	{ path: '/', element: <DashboardView /> },
	{ path: '/top-personnage', element: <TopPerso /> },
	{
		path: '*',
		element: <PageNotFound />,
	},
];
export const menuLink: ListOfPage[] = [
	{ titre: 'Accueil', lien: '/' },
	{
		titre: 'Top personnages',
		lien: '/top-personnage',
	},
];
