import React from 'react';

import styled from 'styled-components';
import { device } from '../../globalStyle/mediaQueryBreakPoints';

const StyledFooter = styled.footer`
	display: flex;
	flex-flow: row wrap;
	justify-content: center;
	background: #6666ff;
	width: auto;
	padding: 1rem;
	@media ${device.mobileL} {
		flex-flow: column wrap;
		text-align: center;
	}
`;

const Footer = () => {
	return (
		<StyledFooter>
			<p>Test technique Start Wars Monisnap</p>
		</StyledFooter>
	);
};

export default Footer;
