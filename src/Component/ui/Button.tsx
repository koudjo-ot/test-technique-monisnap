import styled from 'styled-components';
import { device } from '../../globalStyle/mediaQueryBreakPoints';

type StyledButtonProps = {
	width?: string;
	margin?: string;
	onClick?: any;
};
const StyledButton = styled.button<StyledButtonProps>`
	display: flex;
	flex-flow: row wrap;
	justify-content: center;
	width: 100%;
	margin: ${({ margin }) => margin || '1rem 0 0 0'};
	background: #6666FF;
	padding: 0.5rem;
	color: #fff;
	font-weight: bold;
	&:hover {
		${({ disabled }) => !disabled && 'background: #343b3e; color: white;'}
	cursor: ${({ disabled }) => (!disabled ? 'pointer' : 'auto')};

	@media ${device.mobileL} {
		background: black;
		color: white;
	}
`;

type ContainerProps = {
	justifyContent?: string;
	width?: string;
	alignSelf?: string;
};

const Container = styled.div<ContainerProps>`
	display: flex;
	flex-flow: row wrap;
	width: ${({ width }) => width || 'auto'};
	align-self: ${({ alignSelf }) => alignSelf || 'auto'};
	justify-content: ${({ justifyContent }) =>
		justifyContent || 'center'};
`;

interface ButtonProps {
	titleButton?: string;
	margin?: string;
	width?: string;
	name?: string;
	handleClick?: any;
	justifyContent?: string;
	disabled?: boolean;
	dataTestid?: string;
}
const Button = ({
	titleButton,
	margin,
	width,
	handleClick,
	justifyContent,
	disabled,
	dataTestid,
}: ButtonProps) => (
	<Container justifyContent={justifyContent} width={width}>
		<StyledButton
			margin={margin}
			onClick={handleClick}
			disabled={disabled}
			data-testid={dataTestid}
		>
			{titleButton}
		</StyledButton>
	</Container>
);

export default Button;
