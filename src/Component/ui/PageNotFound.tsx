import React from 'react';
import { Link } from 'react-router-dom';

const PageNotFound = () => {
	return (
		<section>
			<p>404</p>
			<Link to="/">Go Home</Link>
		</section>
	);
};

export default PageNotFound;
