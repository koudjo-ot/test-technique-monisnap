import React, { useState, ReactNode } from 'react';
import { menuLink, ListOfPage } from '../../Info/allPage';
import styled from 'styled-components';
import { device } from '../../globalStyle/mediaQueryBreakPoints';
import Burger from '../../icon/menu-burger/Burger';
import { Link } from 'react-router-dom';

const MenuContainer = styled.nav`
	display: flex;
	flex-flow: row wrap;
	background: #6666ff;
	box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px,
		rgba(0, 0, 0, 0.3) 0px 30px 60px -30px,
		rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset;
	justify-content: center;
	margin: 0 0 1rem 0;
	@media ${device.mobileL} {
		justify-content: left;
	}
`;

const Elements = styled.ul`
	display: flex;
	flex-flow: row wrap;
	gap: 4rem;
	list-style: none;
	@media ${device.mobileL} {
		gap: unset;
		display: none;
	}
`;

const StyledLi = styled.li`
	padding: 1rem;
`;
const StyledLink = styled(Link)`
	color: #fff;
	text-decoration: none;
	font-size: 1.2rem;
	font-weight: 800;
`;

const ResponsiveContainer = styled.ul`
	display: none;
	margin: 0;
	padding: 0;
	@media ${device.mobileL} {
		display: flex;
		flex-flow: column wrap;
		list-style: none;
		width: 100vw;
	}
	svg {
		margin: 2rem;
	}
`;

const BoxOverLay = styled.div`
	display: flex;
	justify-content: center;
	flex-flow: column wrap;
	align-content: center;
	gap: 1rem;
`;

const displayMenuElement = (allMenuLinks: ListOfPage[]): ReactNode =>
	allMenuLinks?.map(({ titre, lien }) => {
		return (
			<StyledLi key={titre}>
				<StyledLink to={lien}>{titre}</StyledLink>
			</StyledLi>
		);
	});

function Menu() {
	const [displayLinkMenu, setDisplayLinkMenu] = useState(false);
	return (
		<MenuContainer>
			<Elements>{displayMenuElement(menuLink)}</Elements>
			<ResponsiveContainer>
				<Burger
					color="#fff"
					handleClick={() =>
						setDisplayLinkMenu(!displayLinkMenu)
					}
				/>
				<BoxOverLay>
					{displayLinkMenu && displayMenuElement(menuLink)}
				</BoxOverLay>
			</ResponsiveContainer>
		</MenuContainer>
	);
}

export default styled(Menu)``;
