import React, { useEffect, useState } from 'react';
import {
	AllCharactersDTO,
	Character,
	getAllCharacters,
	updateVoteById,
} from '../../service/axiosRequest';
import styled from 'styled-components';
import { device } from '../../globalStyle/mediaQueryBreakPoints';
import Button from '../ui/Button';
import {
	filterCharacterByVotes,
	Order,
} from '../../module/filterCharacterByVotes';
import Loading from '../loading/Loading';

const Wrapper = styled.div`
	display: flex;
	flex-flow: row wrap;
	justify-content: space-around;
`;

const Card = styled.div`
	display: flex;
	flex-flow: column wrap;
	justify-content: space-between;
	border: solid white 1px;
	margin: 1rem;

	@media ${device.mobileL} {
		width: 100vw;
	}
`;

const ImgContent = styled.div`
	display: flex;
	justify-content: center;
`;
const WrapperButton = styled.div`
	display: flex;
	justify-content: center;
	padding-bottom: 1rem;
`;

const Title = styled.h2`
	padding: 1rem;
`;

const TopPerso = () => {
	const asc = { asc: false, title: 'ascending' };
	const [loading, setLoading] = useState(false);
	const [listAllCharacters, setListAllCharacters] = useState([]);
	const [sorBy, setSortBy] = useState(asc);
	const starWarsCharacters = async () => {
		try {
			setLoading(true);
			const { status, data }: AllCharactersDTO =
				await getAllCharacters();
			if (status === 200 && data) {
				setListAllCharacters(
					filterCharacterByVotes(data, {
						asc: true,
						title: 'ascending',
					}),
				);
				setTimeout(() => {
					setLoading(false);
				}, 500);
			}
		} catch (error) {
			console.error(error);
			setLoading(false);
		}
	};

	useEffect(() => {
		starWarsCharacters();
	}, []);

	const characterAddOnePoint = async (id: number) => {
		try {
			const { status } = await updateVoteById(id);
			const isAscending = sorBy?.asc;
			isAscending
				? setSortBy({ asc: false, title: 'ascending' })
				: setSortBy({ asc: true, title: 'descending' });

			if (status === 204) {
				starWarsCharacters();
			}
		} catch (error) {
			console.log(error);
		}
	};

	const handleChangeSorBy = (sortChar: Order, list: any) => {
		const isAscending = sortChar?.asc;
		setListAllCharacters(filterCharacterByVotes(list, sortChar));

		isAscending
			? setSortBy({ asc: false, title: 'ascending' })
			: setSortBy({ asc: true, title: 'descending' });
	};
	const displayBattleCharacters = (list: any) => {
		return list?.map(
			({ id, name, pic, homeworld, votes }: Character) => (
				<Card key={id} data-test="cardCharacters">
					<p data-testid={`${id}-name`}>{name}</p>
					<ImgContent>
						<img
							src={pic}
							alt={name}
							width="155px"
							height="205px"
							data-testid={`${id}-picture`}
						/>
					</ImgContent>
					<p>Planete : {homeworld}</p>
					<p data-testid={`${id}-voteNb`}>
						{votes} votes !
					</p>
					<WrapperButton>
						<Button
							titleButton={'+1'}
							dataTestid={`${id}-voteUp`}
							handleClick={() =>
								characterAddOnePoint(id)
							}
							width="8rem"
						/>
					</WrapperButton>
				</Card>
			),
		);
	};
	if (loading) {
		return <Loading />;
	}
	return (
		<section>
			<Title>Classement de vos personnages préférés !</Title>
			<WrapperButton>
				<Button
					titleButton={sorBy?.title}
					dataTestid="sortCharacter"
					handleClick={() =>
						handleChangeSorBy(sorBy, listAllCharacters)
					}
					width="8rem"
				/>
			</WrapperButton>
			<Wrapper>
				{displayBattleCharacters(listAllCharacters)}
			</Wrapper>
		</section>
	);
};

export default TopPerso;
