import React, { useEffect, useState } from 'react';
import {
	Character,
	AllCharactersDTO,
	getAllBattles,
	updateVoteById,
} from '../../service/axiosRequest';
import styled from 'styled-components';
import { device } from '../../globalStyle/mediaQueryBreakPoints';
import Button from '../ui/Button';
import Loading from '../loading/Loading';
import generateId from '../../module/generateId';
const Container = styled.section``;
const Wrapper = styled.div`
	display: flex;
	flex-flow: row wrap;
	justify-content: center;
`;

const Card = styled.div`
	display: flex;
	flex-flow: column wrap;
	justify-content: space-between;

	@media ${device.mobileL} {
		width: 33vw;
	}
`;

const ImgContent = styled.div`
	display: flex;
	justify-content: center;
	height: 405px;
	width: 302px;
	@media ${device.mobileL} {
		width: 33vw;
		height: 43vw;
	}
	@media ${device.desktopS} {
		width: 33vw;
		height: 43vw;
	}
`;

const WrapperButton = styled.div`
	display: flex;
	justify-content: center;
	padding-bottom: 1rem;
`;

const WrapperCard = styled.div`
	display: flex;
	justify-content: center;
	border: solid white 1px;
	gap: 2rem;
	margin-bottom: 2rem;
	flex-flow: row wrap;
	margin: 1rem;
	padding: 1rem;
	@media ${device.mobileL} {
		justify-content: center;
		width: unset;
		min-width: 320px;
	}
`;

const DuelText = styled.span`
	color: #fff;
	font-weight: 500;
	font-size: 4rem;
	margin-bottom: -20px;
	position: relative;
	margin-bottom: -39px;
	background: red;
	height: fit-content;
	@media ${device.tablet} {
		font-size: 2rem;
		right: 0;
		bottom: 38vh;
	}
	@media ${device.mobileS} {
		font-size: 2rem;
		right: 0;
		bottom: 49vh;
	}
	@media screen and (min-width: 661px) {
		display: none;
	}
`;

const TextP = styled.p`
	min-height: 50px;
	align-items: center;
	display: flex;
	justify-content: center;
	background: black;
	color: white;
	width: 7rem;
`;

const Title = styled.h2`
	padding: 1rem;
`;

const DashboardView = () => {
	const [listAllCharacters, setListAllCharacters] = useState([]);
	const [loading, setLoading] = useState(false);
	const [disableClick, setDisableClick] = useState(false);
	const starWarsCharacters = async () => {
		setLoading(true);
		try {
			const { status, data }: AllCharactersDTO =
				await getAllBattles();

			if (status === 200 && data) {
				setListAllCharacters(data);
				setLoading(false);
			}
		} catch (error) {
			console.error(error);
			setLoading(false);
		}
	};

	useEffect(() => {
		if (!loading) {
			starWarsCharacters();
		}
	}, []);

	const characterAddOnePoint = async (
		id: number,
		indexBattle: number,
		disableBtn: any,
	) => {
		try {
			disableBtn(true);
			const { status } = await updateVoteById(id);
			if (status === 204) {
				const updateVotedChar = listAllCharacters[
					indexBattle
				].find((char: any) => char.id === id);
				updateVotedChar.votes += 1;
				setListAllCharacters((oldArray) => [
					...oldArray,
					listAllCharacters,
				]);
				setTimeout(() => {
					disableBtn(false);
				}, 500);
			}
		} catch (error) {
			setLoading(false);
		}
	};
	const displayBattleCharacters = (list: any) => {
		return list?.map((battle: any, index: number) => (
			<WrapperCard
				key={generateId(index)}
				data-testid={`${index}-battle`}
				data-test="cardCharactersBattle"
			>
				{battle.map(
					({
						id,
						name,
						pic,
						homeworld,
						votes,
					}: Character) => (
						<Card key={generateId(id)}>
							<TextP data-testid={`${id}-name`}>
								{name}
							</TextP>
							<ImgContent>
								<img
									src={pic}
									alt={name}
									width="100%"
									height="100%"
								/>
							</ImgContent>
							<p>Planete : {homeworld}</p>
							<p data-testid={`${id}-voteNb`}>
								{votes} votes !
							</p>
							<WrapperButton>
								<Button
									titleButton={'+1'}
									dataTestid={`${id}-voteUp`}
									width="7rem"
									disabled={disableClick}
									handleClick={() =>
										characterAddOnePoint(
											id,
											index,
											setDisableClick,
										)
									}
								/>
							</WrapperButton>
						</Card>
					),
				)}
				<DuelText>Versus</DuelText>
			</WrapperCard>
		));
	};
	if (loading) {
		return <Loading />;
	}
	return (
		<Container>
			<Title>Votez pour votre personnage préféré !</Title>
			<Wrapper>
				{displayBattleCharacters(listAllCharacters)}
			</Wrapper>
		</Container>
	);
};

export default DashboardView;
