import styled from 'styled-components';
import React from 'react';
import DeathStar from '../../icon/img/Death-Star.png';

const LoadingImg = styled.img`
	position: absolute;
	top: 50%;
	left: 50%;
	width: 120px;
	height: 120px;
	margin: -60px 0 0 -60px;
	-webkit-animation: spin 4s linear infinite;
	-moz-animation: spin 4s linear infinite;
	animation: spin 4s linear infinite;
	@-moz-keyframes spin {
		100% {
			-moz-transform: rotate(360deg);
		}
	}
	@-webkit-keyframes spin {
		100% {
			-webkit-transform: rotate(360deg);
		}
	}
	@keyframes spin {
		100% {
			-webkit-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
`;

const Wrapper = styled.div`
	display: flex;
	flex-flow: row wrap;
	justify-content: center;
`;

const WrapperLoading = styled.section`
	min-height: 100vh;
`;

const Loading = () => (
	<WrapperLoading>
		<Wrapper>
			<LoadingImg src={DeathStar} />
		</Wrapper>
	</WrapperLoading>
);

export default Loading;
