import axios from 'axios';

export interface Character {
	id: number;
	name: string;
	pic: string;
	homeworld: string;
	votes: number;
}

export interface AllCharactersDTORESP {
	status: number;
	characters: [Character];
}

export interface AllCharactersDTO {
	data: [Character];

	status: number;
}

export const getAllCharacters = async () => {
	const { data, status }: AllCharactersDTO = await axios?.get(
		`${process.env.REACT_APP_API_LOCAL}/characters`,
		{
			headers: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
			},
		},
	);
	return { data, status };
};

export const getAllBattles = async () => {
	const { data, status }: AllCharactersDTO = await axios?.get(
		`${process.env.REACT_APP_API_LOCAL}/characters/battles`,
		{
			headers: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
			},
		},
	);
	return { data, status };
};

export const updateVoteById = async (id: number) => {
	const { status }: any = await axios?.put(
		`${process.env.REACT_APP_API_LOCAL}/characters/vote/${id}`,
		{
			headers: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
			},
		},
	);
	return { status };
};
