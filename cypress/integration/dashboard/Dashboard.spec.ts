/// <reference types="cypress" />

import * as fakeCharactersList from '../../fixtures/intercept/interceptFixture.json';

describe('HOME - CHARACTERS BATTLE', () => {
	it('should display all characters', () => {
		cy.intercept('GET', '/characters/battles', {
			fixture: 'intercept/interceptFixture.json',
		}).as('battleCharacters');
		cy.visit('http://localhost:3000/');
		cy.get(
			`[data-testid="${fakeCharactersList[0][0]?.id}-name"]`,
		).should('have.text', `${fakeCharactersList[0][0]?.name}`);
		cy.get(
			`[data-testid="${fakeCharactersList[0][0]?.id}-voteUp"]`,
		).should('be.visible');
		cy.get(
			`[data-testid="${fakeCharactersList[0][0]?.id}-voteNb"]`,
		).should('be.visible');
		cy.get(
			`[data-testid="${fakeCharactersList[0][0]?.id}-voteUp"]`,
		).should('be.visible');

		cy.get(
			`[data-testid="${fakeCharactersList[0][0]?.id}-name"]`,
		).should('have.text', `${fakeCharactersList[0][0]?.name}`);
		cy.get(
			`[data-testid="${fakeCharactersList[0][0]?.id}-voteNb"]`,
		).should('be.visible');
		cy.get('[data-test="cardCharactersBattle"]').should(
			'have.length',
			fakeCharactersList.length,
		);
	});
});
