/// <reference types="cypress" />

import { fakeCharactersList } from '../../fakeData/fakeData';

describe('TOP CHARACTERS', () => {
	it('should display all characters', () => {
		cy.intercept('GET', '/characters', {
			fixture: 'intercept/fakeCharactersList.json',
		}).as('topCharacters');
		cy.visit('http://localhost:3000/top-personnage');

		cy.get(`[data-testid="sortCharacter"]`).should('be.visible');
		cy.get('[data-test="cardCharacters"]').should(
			'have.length',
			fakeCharactersList.length,
		);
		cy.get(
			`[data-testid="${fakeCharactersList[0]?.id}-name"]`,
		).should('have.text', `${fakeCharactersList[0]?.name}`);
		cy.get(
			`[data-testid="${fakeCharactersList[0]?.id}-picture"]`,
		).should('have.attr', 'src', `${fakeCharactersList[0]?.pic}`);
		cy.get(
			`[data-testid="${fakeCharactersList[0]?.id}-voteNb"]`,
		).should(
			'have.text',
			`${fakeCharactersList[0]?.votes} votes !`,
		);
		cy.get(
			`[data-testid="${fakeCharactersList[0]?.id}-voteUp"]`,
		).should('be.visible');
	});
});
